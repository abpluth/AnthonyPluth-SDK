const LOTR = require('../src')

global.fetch = jest.fn(
    () =>
        new Promise(resolve => {
            resolve({
                json: () =>
                    Promise.resolve({
                        docs: [],
                    }),
            })
        }),
)

describe('Movie', () => {
    let client

    beforeAll(() => {
        client = new LOTR('TEST-API-TOKEN')
    })
    afterEach(() => {
        fetch.mockClear()
    })

    describe('getAll', () => {
        test('Fetches all movies', async () => {
            await client.movies.getAll()
            expect(fetch).toHaveBeenCalledTimes(1)
            expect(fetch).toHaveBeenCalledWith(
                'https://the-one-api.dev/v2/movie',
                { headers: { Authorization: 'Bearer TEST-API-TOKEN' } },
            )
        })

        test('Encodes all pagination, sorting, filtering options', async () => {
            await client.movies.getAll({
                pagination: { limit: 100, page: 2, offset: 3 },
                sorting: { name: 'asc' },
                filtering: {
                    equals: { a: 1, b: 2 },
                    doesNotEqual: { c: 1, d: 2 },
                    includes: { e: [1, 2, 3], f: [4, 5, 6] },
                    excludes: { e: [1, 2, 3], f: [4, 5, 6] },
                    regex: { name: /foot/i },
                    lessThan: {
                        budgetInMillions: 100,
                    },
                    lessThanOrEqual: {
                        runtimeInMinutes: 200,
                    },
                    greaterThan: {
                        academyAwardWins: 2,
                    },
                    greaterThanOrEqual: {
                        runtimeInMinutes: 160,
                    },
                    exists: 'name',
                    doesNotExist: 'blah',
                },
            })
            expect(fetch).toHaveBeenCalledTimes(1)
            expect(fetch).toHaveBeenCalledWith(
                'https://the-one-api.dev/v2/movie?limit=100&page=2&offset=3&sort=name:asc&a=1&b=2&c!=1&d!=2&e=1,2,3&f=4,5,6&e!=1,2,3&f!=4,5,6&name=/foot/i&budgetInMillions<100&runtimeInMinutes<=200&academyAwardWins>2&runtimeInMinutes>=160&name&!blah',
                { headers: { Authorization: 'Bearer TEST-API-TOKEN' } },
            )
        })
    })

    test('getById', async () => {
        await client.movies.getById('5cd95395de30eff6ebccde5a')
        expect(fetch).toHaveBeenCalledTimes(1)
        expect(fetch).toHaveBeenCalledWith(
            'https://the-one-api.dev/v2/movie/5cd95395de30eff6ebccde5a',
            {
                headers: { Authorization: 'Bearer TEST-API-TOKEN' },
            },
        )
    })

    describe('getQuotes', () => {
        test('Fetches all quotes from movie', async () => {
            await client.movies.getQuotes('5cd95395de30eff6ebccde5c')
            expect(fetch).toHaveBeenCalledTimes(1)
            expect(fetch).toHaveBeenCalledWith(
                'https://the-one-api.dev/v2/movie/5cd95395de30eff6ebccde5c/quote',
                { headers: { Authorization: 'Bearer TEST-API-TOKEN' } },
            )
        })

        test('Pagination options', async () => {
            await client.movies.getQuotes('5cd95395de30eff6ebccde5c', {
                pagination: { limit: 100, page: 2, offset: 3 },
                sorting: { name: 'asc' },
                filtering: {
                    equals: { a: 1, b: 2 },
                    doesNotEqual: { c: 1, d: 2 },
                    includes: { e: [1, 2, 3], f: [4, 5, 6] },
                    excludes: { e: [1, 2, 3], f: [4, 5, 6] },
                    regex: { name: /foot/i },
                    lessThan: {
                        budgetInMillions: 100,
                    },
                    lessThanOrEqual: {
                        runtimeInMinutes: 200,
                    },
                    greaterThan: {
                        academyAwardWins: 2,
                    },
                    greaterThanOrEqual: {
                        runtimeInMinutes: 160,
                    },
                    exists: 'name',
                    doesNotExist: 'blah',
                },
            })
            expect(fetch).toHaveBeenCalledTimes(1)
            expect(fetch).toHaveBeenCalledWith(
                'https://the-one-api.dev/v2/movie/5cd95395de30eff6ebccde5c/quote?limit=100&page=2&offset=3&sort=name:asc&a=1&b=2&c!=1&d!=2&e=1,2,3&f=4,5,6&e!=1,2,3&f!=4,5,6&name=/foot/i&budgetInMillions<100&runtimeInMinutes<=200&academyAwardWins>2&runtimeInMinutes>=160&name&!blah',
                { headers: { Authorization: 'Bearer TEST-API-TOKEN' } },
            )
        })
    })
})
