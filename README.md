# The-One-SDK
A node.js SDK for [The-One-Api](https://the-one-api.dev)

## How To Install
```bash
npm install @abpluth/lotr_sdk
```

## Usage

### Initializing the client
An API token for [the-one-api.dev](https://the-one-api.dev/) is required to use the SDK. Replace `<YOUR-API-TOKEN>` with your actual token.
```javascript
const LOTR = require('@abpluth/lotr_sdk')

const client = new LOTR('<YOUR-API-TOKEN>')
```

### Movies
```javascript
const movies = await client.movies.getAll()
const movie = await client.movies.getById('<movie-id>')
const quotes = await client.movies.getQuotes('<movie-id>')
```
The `getAll` and `getQuotes` functions support the use of pagination, sorting, and filtering. An example showing the use of all possible options is shown below:
```javascript
const quotes = await client.movies.getQuotes('<movie-id>',{
  pagination: { limit: 100, page: 2, offset: 3 },
  sorting: { name: 'asc' },
  filtering: {
    equals: { movie: 'The Two Towers' },
    doesNotEqual: { movie: 'The Fellowship of the Ring' },
    includes: { race: ['Hobbit', 'Human'] },
    excludes: { race: ['Orc', 'Goblin'] },
    regex: { name: /foot/i },
    lessThan: {
      budgetInMillions: 100,
    },
    lessThanOrEqual: {
      runtimeInMinutes: 200,
    },
    greaterThan: {
      academyAwardWins: 2,
    },
    greaterThanOrEqual: {
      runtimeInMinutes: 160,
    },
    exists: 'name',
    doesNotExist: 'someFieldThatDoesntExist,
  },
})
```


## Testing
```bash
npm test
```

## Future Improvements
- [ ] Add additional API endpoints
- [ ] Add caching
- [ ] Add error handling for invalid pagination/sorting/filtering params