# Design

This SDK is designed to be a lightweight (zero-dependency) wrapper around [The-One-Api](https://the-one-api.dev). The client is designed to be used as a singleton and is initialized with the auth token for the API.

The client is also designed to allow for future expansion by supporting all the API endpoints offered by the-one-api. As such, we use a separate class (and file) for each API endpoint and bring them together in `index.js`.

