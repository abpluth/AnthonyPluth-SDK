const Movies = require('./movie')

const BASE_URL = 'https://the-one-api.dev/v2'

class LOTR {
    #apiToken

    constructor(apiToken) {
        this.#apiToken = apiToken
        this.movies = new Movies(BASE_URL, this.#apiToken)
    }
}

module.exports = LOTR
