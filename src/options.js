const operationMap = new Map([
    ['equals', '='],
    ['doesNotEqual', '!='],
    ['includes', '='],
    ['excludes', '!='],
    ['regex', '='],
    ['lessThan', '<'],
    ['lessThanOrEqual', '<='],
    ['greaterThan', '>'],
    ['greaterThanOrEqual', '>='],
    ['exists', ''],
    ['doesNotExist', '!'],
])

function urlEncodeOptions(options) {
    if (!Object.keys(options).length) return ''
    const params = []

    if (options?.pagination) {
        if (options.pagination?.limit)
            params.push(`limit=${options.pagination?.limit}`)
        if (options.pagination?.page)
            params.push(`page=${options.pagination?.page}`)
        if (options.pagination?.offset)
            params.push(`offset=${options.pagination?.offset}`)
    }

    if (options?.sorting) {
        const [key, value] = Object.entries(options.sorting)[0]
        params.push(`sort=${key}:${value}`)
    }

    if (options?.filtering) {
        Object.keys(options.filtering).forEach(option => {
            switch (option) {
                case 'includes':
                case 'excludes':
                    Object.entries(options.filtering[option]).forEach(
                        ([key, value]) => {
                            params.push(
                                `${key}${operationMap.get(option)}${value.join(
                                    ',',
                                )}`,
                            )
                        },
                    )
                    break
                case 'exists':
                case 'doesNotExist':
                    params.push(
                        `${operationMap.get(option)}${
                            options.filtering[option]
                        }`,
                    )
                    break
                default:
                    if (!operationMap.has(option)) {
                        throw new Error(
                            `Invalid filtering operation: ${option}`,
                        )
                    }

                    Object.entries(options.filtering[option]).forEach(
                        ([key, value]) => {
                            params.push(
                                `${key}${operationMap.get(option)}${value}`,
                            )
                        },
                    )
            }
        })
    }

    return `?${params.join('&')}`
}

module.exports = urlEncodeOptions
