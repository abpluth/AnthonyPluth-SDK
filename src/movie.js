const urlEncodeOptions = require('./options')

class Movies {
    constructor(baseUrl, apiToken) {
        this.baseUrl = baseUrl
        this.apiToken = apiToken
    }

    /**
     * Gets all movies
     *
     * @param {object} [options={}] - pagination/sorting/filtering options
     * @returns Promise<object[]>
     */
    getAll(options = {}) {
        return fetch(`${this.baseUrl}/movie${urlEncodeOptions(options)}`, {
            headers: {
                Authorization: `Bearer ${this.apiToken}`,
            },
        }).then(response => response.json())
    }

    /**
     * Gets a specific movie by id
     *
     * @param {string} id - movie id
     * @returns Promise<object[]>
     */
    getById(id) {
        return fetch(`${this.baseUrl}/movie/${id}`, {
            headers: {
                Authorization: `Bearer ${this.apiToken}`,
            },
        })
            .then(response => response.json())
            .then(data => data.docs)
    }

    /**
     * Gets movie quotes for specific movie
     *
     * @param {string} id
     * @param {object} [options={}] - pagination/sorting/filtering options
     * @returns Promise<object[]>
     */
    getQuotes(id, options = {}) {
        return fetch(
            `${this.baseUrl}/movie/${id}/quote${urlEncodeOptions(options)}`,
            {
                headers: {
                    Authorization: `Bearer ${this.apiToken}`,
                },
            },
        ).then(response => response.json())
    }
}

module.exports = Movies
